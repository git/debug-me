{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Main where

import CmdLine
import Graphviz
import Replay
import Verify
import Server
import ControlWindow
import qualified Role.User
import qualified Role.Developer
import qualified Role.Downloader
import qualified Role.Watcher

import Network.Socket
import System.Exit

main :: IO ()
main = withSocketsDo $ do
	c <- getCmdLine
	case mode c of
		UserMode o -> Role.User.run o >>= exitWith
		DeveloperMode o -> Role.Developer.run o
		DownloadMode o -> Role.Downloader.run o
		WatchMode o -> Role.Watcher.run o
		GraphvizMode o -> graphviz o
		ReplayMode o -> replay o
		VerifyMode o -> verify o
		ServerMode o -> server o
		ControlMode o -> controlWindow o
