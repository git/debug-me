{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module DotDir where

import System.Posix
import System.Directory
import System.FilePath

dotDir :: IO FilePath
dotDir = do
	home <- homeDirectory <$> (getUserEntryForID =<< getEffectiveUserID)
	let dir = home </> ".debug-me"
	createDirectoryIfMissing False dir
	return dir
