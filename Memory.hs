{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Memory where

-- | Class of things whose size in memory is known.
--
-- (This can be an approximate size, but should be no larger than the
-- memory used to reprecent the thing.)
class DataSize t where
	-- ^ Size in bytes
	dataSize :: t -> Integer
