{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Output where

import Data.Char

sanitizeForDisplay :: String -> String
sanitizeForDisplay = map go
  where
	go c
		| isPrint c = c
		| otherwise = '?'
