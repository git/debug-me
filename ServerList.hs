{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module ServerList where

import Network.URI
import Data.Maybe

defaultServerUrl :: URI
defaultServerUrl = fromMaybe (error "internal url parse error") $
	parseURI "http://debug-me.joeyh.name:8081/"
