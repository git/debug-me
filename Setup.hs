{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

{-# OPTIONS_GHC -fno-warn-tabs #-}

import Distribution.Simple
import Distribution.Simple.LocalBuildInfo
import Distribution.Simple.Setup
import Distribution.Simple.Utils (installOrdinaryFiles, rawSystemExit)
import Distribution.PackageDescription (PackageDescription(..))
import Distribution.Verbosity (Verbosity)
import System.Info
import System.FilePath

main :: IO ()
main = defaultMainWithHooks simpleUserHooks
	{ postCopy = myPostCopy
	}

myPostCopy :: Args -> CopyFlags -> PackageDescription -> LocalBuildInfo -> IO ()
myPostCopy _ flags pkg lbi = if System.Info.os /= "mingw32"
	then installManpages dest verbosity pkg lbi
	else return ()
  where
	dest = fromFlag $ copyDest flags
	verbosity = fromFlag $ copyVerbosity flags

{- See http://www.haskell.org/haskellwiki/Cabal/Developer-FAQ#Installing_manpages -}
installManpages :: CopyDest -> Verbosity -> PackageDescription -> LocalBuildInfo -> IO ()
installManpages copyDest verbosity pkg lbi =
	installOrdinaryFiles verbosity dstManDir [(".", "debug-me.1")]
  where
	dstManDir   = mandir (absoluteInstallDirs pkg lbi copyDest) </> "man1"
