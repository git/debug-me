{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

{-# LANGUAGE OverloadedStrings, RankNTypes, FlexibleContexts #-}

module Session where

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8
import System.Exit
import Data.Monoid
import Prelude

startSession :: B.ByteString
startSession = "** debug-me session started"

endSession :: ExitCode -> B.ByteString
endSession ec = "** debug-me session ended (" <> B8.pack (show n) <> ")"
  where
	n = case ec of
		ExitSuccess -> 0
		ExitFailure c -> c
