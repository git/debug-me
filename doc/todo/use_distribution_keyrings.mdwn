In addition to the web-of-trust checking debug-me already does, it could also inform the user whether keys are present in distribution keyrings, such as `/usr/share/keyrings/debian-keyring.gpg`.  This would be especially relevant when it is distribution issues that are to be debugged with debug-me: the person connecting is also capable of pushing updates to the usre's machine.

Example output: `Sean Whitton is an official Debian Developer (information accurate as of YYYY-MM-DD)` where the date comes from the version of the `debian-keyring` package.

Distribution packagers of debug-me could add the keyrings to be checked in this way to a configuration file, or possibly just hardcode them somewhere in debug-me's source.

--spwhitton

> [[done]]; you'll need to include the symlinks to the debian keyring
> in the keysafe.deb. --[[Joey]]
