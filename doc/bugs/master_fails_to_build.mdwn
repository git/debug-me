```
Downloading lts-9.0 build plan ...
Downloaded lts-9.0 build plan.
Updating package index Hackage (mirrored at https://s3.amazonaws.com/hackage.fpcomplete.com/) ...
Selected mirror https://s3.amazonaws.com/hackage.fpcomplete.com/
Downloading root
Selected mirror https://s3.amazonaws.com/hackage.fpcomplete.com/
Downloading timestamp
Downloading snapshot
Downloading mirrors
Cannot update index (no local copy)
Downloading index
Updated package list downloaded
Populating index cache ...
Populated index cache.

Error: While constructing the build plan, the following exceptions were encountered:

In the dependencies for debug-me-1.20170810:
    posix-pty must match (>=0.2.1), but the stack configuration has no specified version (latest applicable is 0.2.1.1)
needed since debug-me-1.20170810 is a build target.

Recommended action: try adding the following to your extra-deps in /builddir/debug-me-1.20170810/stack.yaml:
- posix-pty-0.2.1.1

You may also want to try the 'stack solver' command
Plan construction failed.
```

I couldn't see posix-pty in the lts package list? Was it erroneously removed from stack.yaml?

> posix-pty is in stack.yaml since last year, so [[done]] --[[Joey]]
