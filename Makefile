# The install target will add this before all paths it writes to.
PREFIX?=

# Can be "stack" or "cabal", or "./Setup" to build and use Setup.hs 
BUILDER?=stack

# Options to pass to the BUILDER.
# Using -j1 may result in a reproducible build.
BUILDEROPTIONS?=

# Propigate flags through ghc to linker and compiler.
ghc_options=$(shell \
	for w in $(LDFLAGS); do \
	       printf -- "-optl%s\n" "$$w"; \
	done; \
	for w in $(CFLAGS); do \
	       printf -- "-optc%s\n" "$$w"; \
	done; \
	for w in $(CPPFLAGS); do \
	       printf -- "-optc-Wp,%s\n" "$$w"; \
	done; \
	)

build:
	rm -f debug-me
	$(MAKE) debug-me

debug-me:
	if [ "$(BUILDER)" = ./Setup ]; then ghc --make Setup; fi
	if [ "$(BUILDER)" = stack ]; then \
		$(BUILDER) build --ghc-options="$(ghc_options)" $(BUILDEROPTIONS); \
	else \
		$(BUILDER) configure --ghc-options="$(ghc_options)"; \
		$(BUILDER) build $(BUILDEROPTIONS); \
	fi
	if [ "$(BUILDER)" = stack ]; then \
		ln -sf $$(stack path --dist-dir)/build/debug-me/debug-me debug-me; \
	else \
		ln -sf dist/build/debug-me/debug-me debug-me; \
	fi

clean:
	if [ "$(BUILDER)" != ./Setup ] && [ "$(BUILDER)" != cabal ]; then $(BUILDER) clean; fi
	rm -rf dist .stack-work tmp
	rm -f debig-me Build/LinuxMkLibs Setup
	find . -name \*.o -exec rm {} \;
	find . -name \*.hi -exec rm {} \;

install: install-files
	useradd --system debug-me
	chmod 700 $(DESTDIR)$(PREFIX)/var/log/debug-me
	chown debug-me:debug-me $(DESTDIR)$(PREFIX)/var/log/debug-me

install-files: debug-me install-mans
	install -d $(DESTDIR)$(PREFIX)/var/log/debug-me
	install -d $(DESTDIR)$(PREFIX)/usr/bin
	install -s -m 0755 debug-me $(DESTDIR)$(PREFIX)/usr/bin/debug-me
	install -d $(DESTDIR)$(PREFIX)/lib/systemd/system
	install -m 0644 debug-me.service $(DESTDIR)$(PREFIX)/lib/systemd/system/debug-me.service
	install -d $(DESTDIR)$(PREFIX)/etc/init.d
	install -m 0755 debug-me.init $(DESTDIR)$(PREFIX)/etc/init.d/debug-me
	install -d $(DESTDIR)$(PREFIX)/etc/default
	install -m 0644 debug-me.default $(DESTDIR)$(PREFIX)/etc/default/debug-me
	install -d $(DESTDIR)$(PREFIX)/usr/share/debug-me/keyring
	install -m 0655 developer-keyring.gpg \
		$(DESTDIR)$(PREFIX)/usr/share/debug-me/keyring/a_debug-me_developer.gpg

install-mans:
	install -d $(DESTDIR)$(PREFIX)/usr/share/man/man1
	install -m 0644 debug-me.1 $(DESTDIR)$(PREFIX)/usr/share/man/man1/debug-me.1

Build/LinuxMkLibs: Build/LinuxMkLibs.hs
	ghc --make $@ -Wall -fno-warn-tabs

LINUXSTANDALONE_DEST=tmp/debug-me
linuxstandalone: debug-me Build/LinuxMkLibs
	rm -rf "$(LINUXSTANDALONE_DEST)"
	mkdir -p tmp
	cp -R standalone/linux/skel "$(LINUXSTANDALONE_DEST)"
	install -d "$(LINUXSTANDALONE_DEST)/bin"
	cp debug-me "$(LINUXSTANDALONE_DEST)/bin/"
	strip "$(LINUXSTANDALONE_DEST)/bin/debug-me"
	./Build/LinuxMkLibs "$(LINUXSTANDALONE_DEST)"
	$(MAKE) install-mans DESTDIR="$(LINUXSTANDALONE_DEST)"
	cd tmp && tar c debug-me | gzip -9 --rsyncable > debug-me-standalone-$(shell dpkg --print-architecture).tar.gz

.PHONY: debug-me
