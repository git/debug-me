{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module VirtualTerminal where

import System.FilePath
import System.Directory
import System.Process
import System.Environment

-- | Finds a virtual terminal program that looks like it will work
-- to run a command with some parameters.
--
-- Note that the parameters are exposed to the shell by some virtual 
-- terminals, but not by others.
runInVirtualTerminal :: String -> String -> [String] -> IO (Maybe CreateProcess)
runInVirtualTerminal title cmd params = do
	path <- getSearchPath
	mdisplay <- lookupEnv "DISPLAY"
	possibles <- case mdisplay of
		Just _ -> return $ do
			p <- path
			c <- xtermcmds
			return (p, c)
		Nothing -> return []
	find possibles
  where
	find [] = return Nothing
	find ((d, (c, ps)):rest) = do
		exists <- doesFileExist (d </> c)
		if exists
			then return $ Just $ proc (d </> c) ps
			else find rest
	
	-- Ordered list; generally xfce user may have gnome stuff
	-- installed, and only fall back to the older terminals when
	-- nothing else is available.
	xtermcmds =
		[ ("xfce4-terminal", std)
		, ("gnome-terminal", ["-e", unwords (cmd:params)])
		, ("xterm", std)
		, ("rxvt", ["-T", title, "-e", cmd])
		]
	std = ["-T", title, "-e", unwords (cmd:params)]
