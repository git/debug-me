{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Role.Watcher where

import Types
import Log
import Pty
import CmdLine
import SessionID
import Crypto
import Role.Developer

import Control.Concurrent.STM
import Control.Concurrent.STM.TMChan

run :: WatchOpts -> IO ()
run = run' watcher . watchUrl

watcher :: TMVar (TVar DeveloperState) -> TMChan (Message Entered) -> TMChan (MissingHashes AnyMessage) -> SessionID -> IO ()
watcher dsv _ichan ochan sid = withSessionLogger (Just "remote") sid $ \logger -> inRawMode $ do
	sk <- genMySessionKey
	(st, startoutput) <- processSessionStart sk ochan logger dsv
	emitOutput startoutput
	watchSessionReadOnly ochan logger st
