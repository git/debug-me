{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module JSON (
	module Data.Aeson,
	Generic,
	sumOptions
) where

import GHC.Generics (Generic)
import Data.Aeson
import qualified Data.Aeson.Types as Aeson

-- | Nicer JSON encoding for sum types.
sumOptions :: Aeson.Options
sumOptions = defaultOptions { Aeson.sumEncoding = Aeson.ObjectWithSingleField }
