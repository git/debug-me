{- Copyright 2017 Joey Hess <id@joeyh.name>
 -
 - Licensed under the GNU AGPL version 3 or higher.
 -}

module Replay where

import Types
import Log
import CmdLine
import Pty

import qualified Data.ByteString as B
import System.IO
import Control.Concurrent.Async
import Control.Concurrent.Thread.Delay

replay :: ReplayOpts -> IO ()
replay opts = do
	hSetBuffering stdin NoBuffering
	withoutEcho $ go =<< streamLog (replayLogFile opts)
  where
	go [] = return ()
	go (Right l:ls) = do
		case loggedMessage l of
			User (ActivityMessage a) -> do
				_ <- realisticDelay (elapsedTime a)
					`race` waitSpaceBar
				B.hPut stdout $ val $ seenData $ activity a
				hFlush stdout
			User (ControlMessage _) -> return ()
			Developer _ -> return ()
		go ls
	go (Left l:_) = error $ "Failed to parse a line of the log: " ++ l

realisticDelay :: ElapsedTime -> IO ()
realisticDelay (ElapsedTime n) 
	| n < 1 = return ()
	| otherwise = delay $ ceiling $ n * 1000000

waitSpaceBar :: IO ()
waitSpaceBar = do
	c <- getChar
	if c == ' '
		then return ()
		else waitSpaceBar
